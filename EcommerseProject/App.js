import React, { Component } from 'react';
import { View, Text,SafeAreaView } from 'react-native';
// import { SafeAreaView } from 'react-native-safe-area-context';
import Main from './src'
import { Provider } from 'react-redux'
import appStore from './src/Redux/store'
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Provider store={appStore}>
            <Main/>
     </Provider>
    );
  }
}

export default App;
