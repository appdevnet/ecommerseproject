import React, { Component } from 'react';
import { View, Text,Dimensions,Image,StyleSheet } from 'react-native';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
componentDidMount(){
    setTimeout(
        () => {
          this.props.navigation.navigate("LoginScreen")
        },
        Platform.OS === 'ios' ? 1000 : 2000,
      );
}
  render() {
    return (
        <View style={styles.container}>
        <Image source={require('../../assets/app_logo.png')} style={styles.logo} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
      },
      logo: {
        alignSelf: 'center',
        width: '60%',
        height: '60%',
        resizeMode: 'contain',
      },
})
export default SplashScreen;
