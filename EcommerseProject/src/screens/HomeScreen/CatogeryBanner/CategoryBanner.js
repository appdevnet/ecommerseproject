import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions,FlatList,Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class CategoryBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
        categoryList:[
            {id:1,name:"fruits"},
            {id:2,name:"vegitables"},
            {id:3,name:"Backery"},
            {id:4,name:"CurryPowders"},
            // {id:5,name:"Wheat"},
            // {id:6,name:"vegitables"},
            // {id:7,name:"Backery"},
            // {id:8,name:"CurryPowders"},
        ]
    };
  }

  render() {
    return (
        <View style={styles.categoryBanner}>
            <FlatList
            style={{flex:1,left:20}}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            scrollEnabled={false}
            horizontal
            data={this.state.categoryList}
            renderItem={({item,index}) =>
            <View style={[styles.categoryItem,{left:(index*12)+5}]}>
                <View style={{flex:2,flexDirection:"column",justifyContent:"center"}}>
                <Image style={{width:35,height:35,alignSelf:"center"}} source={require('../../../assets/icons/vegetable.png')}></Image>
                </View>
                <View style={{flex:1}}>
                <Text style={{alignSelf:"center",fontSize:10}}>{item.name}</Text>
                </View>
            </View>
            }
            />
            
           
        </View>
    );
  }
}
const styles=StyleSheet.create({
    categoryItem:{
        shadowOffset:{
            width:0,
            height:0
        },
        shadowOpacity:0.4,
        shadowRadius:6,
        flex:1,
        height:width/4,
        width:width/5,
        backgroundColor:"white",
        top:width/22,
        borderRadius:10,
    },
    categoryBanner:{
        flexDirection:"row",
        flex:1,
        height:width/3,
        // backgroundColor:"green",
        top:width/10,
        justifyContent:'space-around'
    },
})
export default CategoryBanner;


// shadow:{
//     height:width/3.5,
//     width:width/4.5,
//     backgroundColor:"#e8e8e8",
//     left:width/22,
//     borderRadius:10,
//     flexDirection:"column",
//     justifyContent:"center"
// },
// categoryItem:{
//     height:width/4,
//     width:width/5,
//     backgroundColor:"white",
//     alignSelf:"center",
    
//     // top:-5,
//     // left:width/22,
//     borderRadius:10,
// },