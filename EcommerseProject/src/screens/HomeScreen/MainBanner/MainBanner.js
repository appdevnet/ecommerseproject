import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions,Image } from 'react-native';
import Swiper from 'react-native-swiper'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
// import p from '../../../assets/images/'
class MainBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        {id:1,url:"https://images.unsplash.com/photo-1583258292688-d0213dc5a3a8?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"},
        {id:2,url:"https://images.unsplash.com/photo-1540340061722-9293d5163008?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80"},
        {id:3,url:"https://images.unsplash.com/photo-1556767576-5ec41e3239ea?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"},
        {id:4,url:"https://images.unsplash.com/photo-1612819052787-618023ea329f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"}, // Network image
        // require('./assets/images/girl.jpg'),          // Local image
      ]
    };
  

  }

  render() {
    return (
        <View style={styles.banner}>
          <Swiper style={{height: 220}} autoplay>
            {this.state.images.map((item)=>
              <Image style={styles.bannerimage} source={{uri:item.url}}></Image>
              )}
          </Swiper>
        </View>
    );
  }
}
const styles=StyleSheet.create({
    bannerimage:{
      borderRadius:10,
      height:"100%",
      width:"100%"
    },
    banner:{
        top:width/20,
        height:height/5,
        width:width/1.12,
        alignSelf:"center",
        backgroundColor:"lightgreen",
        borderRadius:10,
    },
})
export default MainBanner;
