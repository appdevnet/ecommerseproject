import React from 'react'
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    Dimensions,
    TouchableOpacity,
    View,
  } from 'react-native';
  import { connect } from 'react-redux';
  import TopNavbar from '../../components/TopNavBar'
  import MainBanner from './MainBanner/MainBanner'
  import CategoryBanner from './CatogeryBanner'
  import HomeProducts from './HomeProducts'
  var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
 class HomeScreen extends React.Component{
     componentDidMount(){
         console.log(this.props.userName,"hai")
     }
    render(){
        return(
            <SafeAreaView style={{backgroundColor:"#f2f2f2",width: '100%',
            height: '100%',}}>
                    <TopNavbar/>
            <ScrollView>
                <View style={styles.loginview}>
                    <MainBanner style={{flex:1,}}/>
                    <CategoryBanner style={{flex:1,}}/>
                    <HomeProducts {...this.props}/>
                    <View style={{height:height/5,width:width/2,alignSelf:"center",}}>

                    </View>
                </View>
            </ScrollView>
            </SafeAreaView>
        )
    }
}
const styles=StyleSheet.create({
    banner:{
        top:width/20,
        height:height/5,
        width:width/1.12,
        alignSelf:"center",
        // backgroundColor:"lightgreen",
        borderRadius:10,
    },
    loginview:{
        // height:height,
        // width:width,
        backgroundColor:"white"
    },
    loginButoon:{
        width:width/3,
        height:height/20,
        backgroundColor:"yellow",
        borderRadius:10,
        alignSelf:"center",
        flexDirection:"column",
        justifyContent:"center"
    }
})
const mapStateToProps = (state) => {
    return {
        userName:state.global.userName
    };
  };
  
  export default connect(mapStateToProps, {
  })(HomeScreen);