import React, { Component } from 'react';
import { View, Text,StyleSheet,FlatList,Image,Dimensions} from 'react-native';
import ProductItem from './ProductItem'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class HomeProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
        categoryList:[
            {id:1,name:"Small Tomato",img_url:"https://bit.ly/3hNvU2l",price:"Rs.10/kg"},
            {id:2,name:"Water Melon",img_url:"https://bit.ly/3fL0Q0A",price:"Rs.30/kg"},
            {id:3,name:"Egg",img_url:"https://bit.ly/2Tj8Myv",price:"Rs.4.50/piece"},
            {id:4,name:"Cucumber",img_url:"https://bit.ly/2RJAtQz",price:"Rs.45/kg"},
            {id:5,name:"Banana",img_url:"https://bit.ly/3vwuhda",price:"Rs.40/kg"},
            {id:6,name:"Garlic",img_url:"https://bit.ly/2RN0A9b",price:"Rs.20/100gm"},
        ]
    };
  }

  render() {
    return (
        <View style={styles.mainView}>
            <View style={styles.headingView}>
                <Text style={styles.Heading}>Featured Products</Text>
            </View>
            <View style={styles.productList}>
            <FlatList
            style={{left:10,top:10}}
            numColumns={2}
            data={this.state.categoryList}
            scrollEnabled={false}
            renderItem={({item,index}) =>
                <ProductItem 
                {...this.props}
                index={index}
                item={item}
                />
            }
            />
            <View style={styles.view_products}>
                <Text style={styles.view_all}>View All</Text>
            </View>
            </View>
        </View>
);
}
}
const styles=StyleSheet.create({
    view_all:{
        alignSelf:"center",
        fontSize:width/25,
        shadowOffset:{
        width:0,
        height:0
    },
    shadowOpacity:0.5,
    shadowRadius:3,},
    view_products:{
        top:10,
        // width:"100%", 
        // backgroundColor:"green",
        height:30,
        flexDirection:"column",
        justifyContent:"center"
    },
headingView:{
flex:1,
// backgroundColor:"green"
},
Heading:{
    top:width/30,
    left:width/30,
    fontSize:width/25,
    fontWeight: "bold"
} ,
productList:{
    flex:10,
    top:-15,
    // backgroundColor:"yellow",
    width:width/1.25,
    alignSelf:"center",
    height:height/100,
},  
mainView:{
    shadowOffset:{
        width:0,
        height:0
    },
    // flex:1,
    shadowOpacity:0.4,
    shadowRadius:6,
    borderRadius:10,
    width:width/1.12,
    height:height/1.23,
    backgroundColor:"white",
    alignSelf:"center",
    top:70
}
})

export default HomeProducts;
