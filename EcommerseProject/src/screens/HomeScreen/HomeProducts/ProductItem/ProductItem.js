import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions,FlatList,Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class ProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    let index=this.props.index
    let data = this.props.item
    return (
        
      <TouchableOpacity onPress={()=>this.props.navigation.navigate("ProductDetailsScreen")}>
            <View style={[styles.mainView,{left:index%2==0?width/95:width/20,top:index%2==0?index*20+5:(index-1)*20+4}]}>
                <View style={styles.productImage}>
                    <Image style={{height:"100%",width:"100%",alignSelf:"center",resizeMode:"contain"}} source={{uri:data.img_url,}}/>
                </View>
                <View style={styles.product_name_view}>
                      <Text style={{alignSelf:"center",fontWeight:'500'}}>{data.name}</Text>
                </View>
                <View style={styles.product_price_view}>
                      <Text style={{alignSelf:"center",fontWeight:'500'}}>{data.price}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
  }
}
const styles=StyleSheet.create({
  productImage:{
height:height/7.5,
width:width/3,
top:width/50,
alignSelf:"center"
  },
mainView:{
    shadowOffset:{
        width:0,
        height:0
    },
    shadowOpacity:0.4,
    shadowRadius:6,
    borderRadius:10,
    width:width/2.8,
    height:height/5,
    backgroundColor:"white"
},
product_name_view:{width:"100%",height:height/35},
product_price_view:{width:"100%",height:height/35,},
})
export default ProductItem;
