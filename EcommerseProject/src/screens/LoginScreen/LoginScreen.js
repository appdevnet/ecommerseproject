import React from 'react'
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    Dimensions,
    TouchableOpacity,
    View,
    Image,
    AsyncStorage
  } from 'react-native';
  import {storeData,getData} from '../../services/Storage'
import { TextInput } from 'react-native-gesture-handler';
import { greaterThan } from 'react-native-reanimated';
import InputBox from './InputBox'

import {
    setUsername
  } from '../../Redux/actions';
  import { connect } from 'react-redux';
  var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class LoginScreen extends React.Component{
    constructor(props){
        super(props);
        state={
            userName:""
        }
    }
    componentDidMount(){
        this.getData()
    }
logIn(){
    this.props.setUsername(this.state.userName)
    this.props.navigation.navigate("HomeScreen",{ name: "response" })
    // storeData("username",this.state.userName)
}
getData(){
    
        getData("username").then((response) => {
            console.log(response,"data")
            if(response!=null&&response!=""&&response!=undefined){
                this.props.navigation.navigate("HomeScreen",{ name: response })
            }
        })
}
    render(){
        return(
            <SafeAreaView>
                <View style={styles.loginview}>
                    <View style={{flex:0.5}}>
                        <Text style={{fontSize:width/10,paddingTop:30,paddingLeft:10}}>
                            Welcome
                        </Text>
                        <Text style={{paddingTop:10,paddingLeft:20}}>Sign in to Continue</Text>
                    </View>
                    <View style={styles.logo}>
                    <Image source={require('../../assets/shop_logo.png')} style={styles.logo_img} />
                    </View>
                    <View style={{flex:2,flexDirection:"column",justifyContent:"center",top:-35}}>
                        <InputBox 
                        placeHolder={"UserName"}
                        onTextChange={(text)=>this.setState({
                            userName:text
                        })}
                        
                        />
                        <View style={{paddingTop:50}}>
                            <InputBox
                            placeHolder={"Password"}
                            onTextChange={(text)=>this.setState({
                                passWord:text
                            })}
                            />
                        </View>
                        <TouchableOpacity 
                        onPress={()=>this.logIn()}
                        style={styles.loginBtn}>
                            <Text style={{alignSelf:"center"}}>LogIn</Text>
                        </TouchableOpacity>
                    </View>
                        
                    <View style={{flex:1,}}>

                    </View>

                    {/* <View style={{height:"100%",flexDirection:"column",justifyContent:"center"}}>
                <TouchableOpacity style={styles.loginButoon} onPress={()=>this.props.navigation.navigate('HomeScreen')}>
                    <View style={styles.loginButoon}>
                        <Text style={{alignSelf:"center"}}>HOME</Text>
                    </View>
                </TouchableOpacity>
                    </View> */}
                </View>
            </SafeAreaView>
        )
    }
}
const styles=StyleSheet.create({
    logo_img:{
        height:"90%",
        width:"90%",
        alignSelf:"center",
        resizeMode: 'contain',
    },
    logo:{
        // backgroundColor:"green",
        height:width/2,
        width:width/2,
        alignSelf:"center",
        flexDirection:"column",
        justifyContent:"center"
    },
    loginBtn:{
        width:width/2,
        height:height/20,
        top:40,
        borderRadius:100,
        backgroundColor:'green',
        flexDirection:'column',
        alignSelf:'center',
        justifyContent:"center"
    },
    loginview:{
        height:height,
        width:width,
        // backgroundColor:"red"
    },
    loginButoon:{
        width:width/3,
        height:height/20,
        // backgroundColor:"green",
        borderRadius:10,
        alignSelf:"center",
        flexDirection:"column",
        justifyContent:"center"
    }
})
const mapStateToProps = (state) => {
    return {
    };
  };
  
  export default connect(mapStateToProps, {
    setUsername,
  })(LoginScreen);