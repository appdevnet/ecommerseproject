import React, { Component } from 'react';
import { View, Text,TextInput,StyleSheet,Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class InputBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
        value:""
    };
  }

  render() {
    return (
      <View style={styles.inputValue}>
        <TextInput 
        // value={this.state.value}
        style={{paddingLeft:25,width:'95%',height:'95%'}}
        placeholderTextColor={"gray"}
        placeholder={this.props.placeHolder}
        onChangeText={(text)=>this.props.onTextChange(text)}
        />
      </View>
    );
  }
}
const styles=StyleSheet.create({
    inputValue: {
        height:height/15,
        alignSelf:"center",
        width:width/1.2,
        borderRadius: 50,
        paddingVertical: 12,
        paddingHorizontal: 10,
        fontSize: width / 24,
        borderWidth: 1,
        borderColor: "gray",
        marginTop: 10,
      },
})
export default InputBox;


