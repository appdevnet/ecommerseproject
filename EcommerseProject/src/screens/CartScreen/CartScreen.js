import React, { Component } from 'react';
import { View, Text,SafeAreaView } from 'react-native';
import CartTopNavbar from './CartTopNavbar';
import CartList from './CartList'
class CartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <SafeAreaView style={{backgroundColor:"white"}}>
          <CartTopNavbar {...this.props}/>
          <CartList/>
        {/* <Text> CartScreen </Text> */}
      </SafeAreaView>
    );
  }
}

export default CartScreen;
