import React, { Component } from 'react';
import { View, Text,Dimensions,StyleSheet,Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class CartTopNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.left}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={[styles.iconView,{left:-10}]}>
                <Image style={[styles.icon]} source={require('../../../assets/icons/back_icon.png')}/>
            </TouchableOpacity>
            <Text style={styles.cartTitle}>
                SHOPPING BAG 
            </Text>
        </View>
        <View style={styles.right}>
            <TouchableOpacity style={[styles.iconView,{width:width/3,borderRadius:10}]} >
            <Text style={styles.wisthlistIcon}>
                WISHLIST 
                <Image resizeMode={"contain"} style={[styles.icon,{height:width/25}]} source={require('../../../assets/icons/heart_filled.png')}/>
            </Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles=StyleSheet.create({
    left:{
            // backgroundColor:"yellow",
            flex:1.5,
            flexDirection:"row",
            justifyContent:"space-evenly",
         },
    right:{
            flexDirection:"row",
            // justifyContent:"space-evenly",
            // backgroundColor:"red",
            flex:1
         },
mainView:{
            width:width,
            backgroundColor:"white",
            height:height/15,
            flexDirection:"row",
         },
iconView:{
                flexDirection:"column",
                justifyContent:"center",
                shadowOffset:{
                    width:0,
                    height:0
                },
                shadowOpacity:0.5,
                shadowRadius:3,
                borderRadius:100,
                height:height/18,
                left:width/40,
                top:width/70,
                width:width/8,
                // ,
                backgroundColor:"white"
               },
icon:{
            alignSelf:"center",
            height:width/17,
            width:width/17
        },
cartTitle:{
    fontSize:width/22,
    alignSelf:"center",
    left:-20,
    fontWeight:"bold"
},
wisthlistIcon:{
    alignSelf:"center",
    fontWeight:"bold"
}
})

export default CartTopNavbar;
