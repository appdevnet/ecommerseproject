
import React, { Component } from 'react';
import { View, Text,Dimensions,StyleSheet,Image,TouchableOpacity,TextInput } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { color } from 'react-native-reanimated';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class CartList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        cartList:[
            {id:1,name:"Small Tomato",img_url:"https://bit.ly/3hNvU2l",price:"Rs.10/kg",count:"13",total:"150"},
            {id:2,name:"Water Melon",img_url:"https://bit.ly/3fL0Q0A",price:"Rs.30/kg",count:"15",total:"450"},
            {id:3,name:"Egg",img_url:"https://bit.ly/2Tj8Myv",price:"Rs.4.50/piece",count:"40",total:"180"},
            {id:4,name:"Cucumber",img_url:"https://bit.ly/2RJAtQz",price:"Rs.45/kg",count:"10",total:"450"},
            {id:5,name:"Banana",img_url:"https://bit.ly/3vwuhda",price:"Rs.40/kg",count:"1",total:"40"},
            {id:6,name:"Garlic",img_url:"https://bit.ly/2RN0A9b",price:"Rs.20/100gm",count:"15",total:"300"},
        ]
    };
  }
  checkQuantity(text){
    if(Number(text)<1){
      this.setState({
          quantity:1
      })
      //   alert("minimum")
        
    }
    else{
      this.setState({
          quantity:text
      })
    }
}
changeQuantity(type){
  let value=this.state.quantity
  type=="add"?this.setState({quantity:String((Number(value)+1))}):
  (
  
      (Number(value)-1)<1?this.setState({quantity:"1"}):  this.setState({quantity:String((Number(value)-1))}))
  
console.log(type)
}
  render() {
    return (
      <View style={{backgroundColor:"white",paddingBottom:70}} >
       
       <View style={style.mainView}>
           <View style={style.imageView}>
               <Image style={style.image} source={{uri:"https://bit.ly/3hNvU2l",}}/>
           </View>
           <View style={style.productInfoView}>
                <Text
                numberOfLines={1}
                 style={{fontSize:width/30,fontWeight:"bold"}}>Small Tomato</Text>
                <Text style={{fontSize:width/35,}}>Rs.45/kg</Text>
                <Text style={{fontSize:width/35,}}>Total : $450</Text>
           </View>
           <View style={style.quantityView}>
                    <View style={{width:width/5.5,height:height/40,backgroundColor:"lightgreen",borderRadius:10,flexDirection:"column",justifyContent:"center"}}>
                        <Text style={{alignSelf:"center"}}>20% Off</Text>
                    </View>
                    <View style={style.productQuantity}>
                <TouchableOpacity onPress={()=>this.changeQuantity("sub")} style={style.iconView}>
                    <Image style={style.icon} source={require('../../../assets/icons/minus_icon.png')}/>
                </TouchableOpacity>
                <View style={[style.iconView,{width:width/7}]}>
                    <TextInput style={style.icon}
                    keyboardType = 'numeric' 
                    onChangeText={(text)=>{this.checkQuantity(text)}}
                    value={this.state.quantity}
                    />
                </View>
                <TouchableOpacity onPress={()=>this.changeQuantity("add")} style={style.iconView}>
                    <Image style={style.icon} source={require('../../../assets/icons/plus_icon.png')}/>
                </TouchableOpacity>
            </View>
           </View>
           </View>   
       <View style={[style.mainView,{top:40}]}></View>   
       <View style={[style.mainView,{top:60}]}></View>   
                {/* <FlatList
                    style={{left:10,top:10}}
                    numColumns={1}
                    data={this.state.cartList}
                    scrollEnabled={true}
                    renderItem={({item,index}) =>
                    <View style={{backgroundColor:"red",width:width/1.5}}>
                        <Text>{item.name}</Text>
                        </View>
                    }
            /> */}
            
      </View>
    );
  }
}
const style=StyleSheet.create({
    imageView : {
        height:height/9,
        width:width/5,top:10
    },
    image:{height:"100%",width:"100%",alignSelf:"center",resizeMode:"contain"},
    mainView : {
        backgroundColor:"white",
        width:width/1.1,
        alignSelf:"center",
        top:20,
        height:height/7.5,
        borderRadius:10,
        flexDirection:"row",
        justifyContent:"space-evenly",
        shadowOffset:{
            width:0,
            height:0
        },
        // flex:1,
        shadowOpacity:0.4,
        shadowRadius:6,
    },
    productInfoView : {
        // backgroundColor:"green",
        height:height/9,
        width:width/2.8,
        top:10,
        flexDirection:"column",
        justifyContent:"space-evenly"
    },
    quantityView  : {
        // backgroundColor:"pink",
        height:height/9,
        width:width/4,
        top:10,
        flexDirection:"column",
        justifyContent:"space-evenly"
    },
    iconView:{
        flexDirection:"column",
        justifyContent:"center",
        shadowOffset:{
            width:0,
            height:0
        },
        shadowOpacity:0.5,
        shadowRadius:3,
        borderRadius:100,
        height:height/35,
        // left:width/40,
        top:width/10,
        width:width/25,
        // ,
        backgroundColor:"white"
       },
icon:{
    alignSelf:"center",
    height:width/46,
    width:width/49,
    alignItems:"center"
},
productQuantity:{
    flex:1,
    // backgroundColor:"blue",
    flexDirection:"row",
    justifyContent:"space-around"
},
})
export default CartList;
