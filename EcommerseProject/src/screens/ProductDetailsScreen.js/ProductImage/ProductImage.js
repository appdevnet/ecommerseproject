import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions,Image,TextInput,TouchableOpacity } from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class ProductImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
        quantity:"1",
    };
  }
  checkQuantity(text){
      if(Number(text)<1){
        this.setState({
            quantity:1
        })
        //   alert("minimum")
          
      }
      else{
        this.setState({
            quantity:text
        })
      }
  }
//   https://bit.ly/2Tj8Myv
// https://bit.ly/3bRYSdr
changeQuantity(type){
    let value=this.state.quantity
    type=="add"?this.setState({quantity:String((Number(value)+1))}):
    (
    
        (Number(value)-1)<1?this.setState({quantity:"1"}):  this.setState({quantity:String((Number(value)-1))}))
    
console.log(type)
}
  render() {
    return (
      <View style={styles.mainView}>
        <Image style={{height:height/3.5,width:width/1.1,alignSelf:"center",resizeMode:"contain"}} source={{uri:"https://bit.ly/2TeV6o5"}}>

        </Image>
        <View style={styles.product_detailsView}>
            <View style={styles.productNameView}>
            <Text style={styles.productName}>Nirapara Chicken Powder</Text>
            <Text style={styles.productPrice}>Rs.10 /piece</Text>
            </View>
            <View style={styles.productQuantity}>
                <TouchableOpacity onPress={()=>this.changeQuantity("sub")} style={styles.iconView}>
                    <Image style={styles.icon} source={require('../../../assets/icons/minus_icon.png')}/>
                </TouchableOpacity>
                <View style={[styles.iconView,{width:width/7}]}>
                    <TextInput style={styles.icon}
                    keyboardType = 'numeric' 
                    onChangeText={(text)=>{this.checkQuantity(text)}}
                    value={this.state.quantity}
                    />
                </View>
                <TouchableOpacity onPress={()=>this.changeQuantity("add")} style={styles.iconView}>
                    <Image style={styles.icon} source={require('../../../assets/icons/plus_icon.png')}/>
                </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}
const styles=StyleSheet.create({
    mainView:{
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        top:width/30,
        alignSelf:"center",
        width:width,
        height:height/2.2,
        backgroundColor:"white"
    },
    product_detailsView:{
        flexDirection:"row",
        // backgroundColor:"green",
        width:width/1.1,
        height:height/7,
        alignSelf:"center"
    },
    productNameView:{
        flex:2,
        // backgroundColor:"red"
    },
    productQuantity:{
        flex:1,
        // backgroundColor:"blue",
        flexDirection:"row",
        justifyContent:"space-around"
    },
    productName:{
        fontSize:width/13.5,
        fontWeight:"500"
    },
    productPrice:{
        top:width/45,
        fontSize:width/22
    },
    iconView:{
        flexDirection:"column",
        justifyContent:"center",
        shadowOffset:{
            width:0,
            height:0
        },
        shadowOpacity:0.5,
        shadowRadius:3,
        borderRadius:100,
        height:height/25,
        // left:width/40,
        top:width/10,
        width:width/15,
        // ,
        backgroundColor:"white"
       },
icon:{
    alignSelf:"center",
    height:width/26,
    width:width/19,
    alignItems:"center"
}

})
export default ProductImage;
