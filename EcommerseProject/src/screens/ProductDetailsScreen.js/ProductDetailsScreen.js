import React, { Component } from 'react';
import { View, Text,SafeAreaView,ScrollView,Dimensions } from 'react-native';
import TopNavbar from '../../components/TopNavBar'
import DetailsTopNavbar from './DetailsTopNavbar'
import ProductImage from './ProductImage'
import Details from './Details'
import BotttomButton from './BottomButton'
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
class ProductDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView style={{backgroundColor:"white"}}>
                <DetailsTopNavbar {...this.props}/>
        <ScrollView style={{height:height/1.3}}>
                <ProductImage/>
                <Details/>
        </ScrollView>
        <BotttomButton/>
        
         </SafeAreaView>
    );
  }
}

export default ProductDetailsScreen;
