import React, { Component } from 'react';
import { View, Text,Dimensions,StyleSheet,Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
// import a from '../../../assets/icons/back_icon.png'
class DetailsTopNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.left}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={styles.iconView}>
                <Image style={styles.icon} source={require('../../../assets/icons/back_icon.png')}/>
            </TouchableOpacity>
        </View>
        <View style={styles.right}>
            <TouchableOpacity style={styles.iconView}>
                <Image style={styles.icon} source={require('../../../assets/icons/share_icon.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconView}>
                <Image style={styles.icon} source={require('../../../assets/icons/heart_filled.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconView} onPress={()=>this.props.navigation.navigate("CartScreen")}>
                <Image style={[styles.icon,{width:width/10,height:width/10}]} source={require('../../../assets/cart_icon.png')}/>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles=StyleSheet.create({
    left:{
            // backgroundColor:"yellow",
            flex:1
         },
    right:{
            flexDirection:"row",
            justifyContent:"space-evenly",
            // backgroundColor:"red",
            flex:1.5
         },
mainView:{
            width:width,
            // backgroundColor:"blue",
            height:height/15,
            flexDirection:"row",
         },
iconView:{
                flexDirection:"column",
                justifyContent:"center",
                shadowOffset:{
                    width:0,
                    height:0
                },
                shadowOpacity:0.5,
                shadowRadius:3,
                borderRadius:100,
                height:height/18,
                left:width/40,
                top:width/70,
                width:width/8,
                // ,
                backgroundColor:"white"
               },
icon:{
            alignSelf:"center",
            height:width/17,
            width:width/17
        }
})
export default DetailsTopNavbar;
