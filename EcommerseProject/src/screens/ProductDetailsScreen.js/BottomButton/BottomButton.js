import React, { Component } from 'react';
import { View, Text,Dimensions,StyleSheet,TouchableOpacity,Image } from 'react-native';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
class BottomButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={styles.mainView}>
            <View style={styles.wishListView}>
                <TouchableOpacity style={styles.iconView}>
                <Text style={styles.addTocart}>WISHLIST</Text>
                            <Image style={[styles.icon,]} source={require('../../../assets/icons/heart_filled.png')}/>
                    </TouchableOpacity>
            </View>
            <View style={styles.cartView}>
                <TouchableOpacity style={styles.iconView}>
                    <Text style={styles.addTocart}>ADD TO CART</Text>
                        <Image style={[styles.icon,{height:width/15,height:width/15}]} source={require('../../../assets/cart_icon.png')}/>
                </TouchableOpacity>
            </View>
        </View>
    );
  }
}
const styles=StyleSheet.create({
mainView:{
    width:"100%",
    height:width/4,
    // backgroundColor:"red",
    flexDirection:"row",

},
wishListView:{
flex:1,
// backgroundColor:"green"
},
cartView:{
flex:1,
// backgroundColor:"blue"
},
iconView:{
    flexDirection:"row",
    justifyContent:"space-evenly",
    shadowOffset:{
        width:0,
        height:0
    },
    // alignSelf:"center",
    shadowOpacity:0.5,
    shadowRadius:3,
    borderRadius:100,
    height:height/18,
    left:width/40,
    top:width/70,
    width:width/2.3,
    backgroundColor:"white",
    right:width,
   },
icon:{
alignSelf:"center",
height:width/17,
width:width/17
},
addTocart:{
    paddingTop:"10%"
}
})
export default BottomButton;
