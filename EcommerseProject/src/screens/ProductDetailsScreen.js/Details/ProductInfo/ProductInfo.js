import React, { Component } from 'react';
import { View, Text,StyleSheet,Dimensions } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class ProductInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
        information:[
            {id:1,attribute:"ProductName",value:"NiraPara Beef Masala "},
            {id:2,attribute:"Brand",value:"NiraPara"},
            {id:3,attribute:"Quantity",value:"100gm"},
            {id:1,attribute:"ProductName",value:"NiraPara Beef Masala"},
            {id:2,attribute:"Brand",value:"NiraPara"},
            {id:3,attribute:"Quantity",value:"100gm"},
            {id:1,attribute:"ProductName",value:"NiraPara Beef Masala"},
            {id:2,attribute:"Brand",value:"NiraPara"},
            {id:3,attribute:"Quantity",value:"100gm"},
            
        ]
    };
  }

  render() {
    return (
      <View style={[styles.mainView,{height:(this.state.information.length)*width/10}]}>
        <FlatList
        scrollEnabled={false}
        data={this.state.information}
        renderItem={({item,index})=>
                        <View style={[styles.attributes,{top:index*width/20}]}>
                            <Text style={styles.attributeText}>{item.attribute}</Text>
                            <Text style={styles.valueText}>{item.value}</Text>
                            {/* <Text style={styles.attributeText}>Quantity</Text> */}
                            
                        </View>
        }/>
      </View>
    );
  }
}
const styles=StyleSheet.create({
    mainView:{
        // backgroundColor:"red",
        paddingLeft:width/55,
        width:width/1.12,
        alignSelf:"center",
        
    },
    attributes:{
        flex:1,
        // backgroundColor:"blue",
        flexDirection:"row",
        justifyContent:"space-around"
    },
    values:{
        flex:1.5,
        flexDirection:"column",
        justifyContent:"space-around"
        // backgroundColor:"green"
    },
    attributeText:{
        fontSize:width/25,
        flexDirection: "column",
        flex: 1
        // flex:1,
    },
    valueText:{
        fontSize:width/25,
        flex:1.5
    }
})
export default ProductInfo;
