import React, { Component } from 'react';
import { View, Text,Dimensions,StyleSheet } from 'react-native';
import ProductInfo from './ProductInfo'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
        description:"So, MealBoard shall store users' recipes, plan their meals, generate relevant shopping lists, synch between various gadgets, etc. By the way, mentioned grocery shopping list app has a web version, so users can enjoy it as well. MealBoard is universal and great for iPhone and Android systems."
    };
  }

  render() {
    return (
        <View style={styles.mainView}>
            <Text style={styles.Title}>Product Details</Text>
                <Text style={styles.description}>{this.state.description}</Text>
            <ProductInfo/>
        </View>
    );
  }
}
const styles=StyleSheet.create({
mainView:{
            width:width/1.05,
            // height:height,
            backgroundColor:"white",
            alignSelf:"center",
            top:-(width/50),
            shadowOffset:{
            width:0,
            height:0
        },
        shadowOpacity:0.5,
        shadowRadius:3,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        paddingBottom:width/5
        },
Title:{
    padding:width/25,
    fontSize:width/20,
    fontWeight:"500"
},
description:{
    paddingTop:0,
    padding:width/25,
    fontSize:width/27
}
})
export default Details;
