const INITAL_STATE = {
 userName:"",
};

export default (state = INITAL_STATE, action) => {

    switch (action.type) {
        case 'set_username':
          return {...state, userName: action.payload};
        default:
            return state;
        }

}