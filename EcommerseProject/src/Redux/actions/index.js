export const setUsername = (username) => {
    return {
      type: 'set_username',
      payload: username,
    };
  };