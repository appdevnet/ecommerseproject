import React, { Component } from 'react';
import { View, Text,StyleSheet,TextInput,Dimensions } from 'react-native';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={styles.inputValue}>
        <TextInput 
        style={{paddingLeft:25,width:'95%',height:'95%'}}
        placeholderTextColor={"gray"}
        placeholder={"Search Grocries or products"}
        onChangeText={(text)=>console.log(text)}
        />
      </View>
    );
  }
}
const styles=StyleSheet.create({
    inputValue: {
        height:height/20,
        alignSelf:"center",
        width:width/1.4,
        borderRadius: 100,
        paddingVertical: 12,
        paddingHorizontal: 10,
        fontSize: width / 24,
        borderWidth: 1,
        borderColor: "gray",
        marginTop: 10,
      },
})
export default SearchBox;
