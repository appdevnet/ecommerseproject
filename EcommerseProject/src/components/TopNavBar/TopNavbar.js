import React, { Component } from 'react';
import { View, Text,StyleSheet,Image } from 'react-native';
import SearchBox from './searchBox'
class TopNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.mainView}>
          <Image source={require('../../assets/menu_icon.png')} style={styles.menu_icon} />
          <View style={{left:5}}>
        <SearchBox/>
        </View>
        <Image source={require('../../assets/cart_icon.png')} style={styles.cart_icon} />
      </View>
    );
  }
}
const styles=StyleSheet.create({
mainView:{
  backgroundColor:"white",
    flexDirection:"row",
    justifyContent:"space-around",
    paddingBottom: 15,
},
cart_icon:{
    top:5,
    height:50,
    width:50,
},
menu_icon:{
    top:20,
    height:22,
    width:22
}
})
export default TopNavbar;
